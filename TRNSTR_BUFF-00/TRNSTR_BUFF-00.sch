EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5E5980BD
P 1350 4950
F 0 "J1" H 1458 5131 50  0000 C CNN
F 1 "Ext_pwr" H 1400 5050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1350 4950 50  0001 C CNN
F 3 "~" H 1350 4950 50  0001 C CNN
	1    1350 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5E59876F
P 1600 5100
F 0 "#PWR06" H 1600 4850 50  0001 C CNN
F 1 "GND" H 1605 4927 50  0000 C CNN
F 2 "" H 1600 5100 50  0001 C CNN
F 3 "" H 1600 5100 50  0001 C CNN
	1    1600 5100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5E598BB8
P 1600 4900
F 0 "#PWR05" H 1600 4750 50  0001 C CNN
F 1 "VCC" H 1617 5073 50  0000 C CNN
F 2 "" H 1600 4900 50  0001 C CNN
F 3 "" H 1600 4900 50  0001 C CNN
	1    1600 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4900 1600 4950
Wire Wire Line
	1600 4950 1550 4950
Wire Wire Line
	1550 5050 1600 5050
Wire Wire Line
	1600 5050 1600 5100
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5E59A9A0
P 1500 5950
F 0 "J2" H 1550 6367 50  0000 C CNN
F 1 "IN" H 1550 6276 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 1500 5950 50  0001 C CNN
F 3 "~" H 1500 5950 50  0001 C CNN
	1    1500 5950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5E59B114
P 1500 6750
F 0 "J3" H 1550 7167 50  0000 C CNN
F 1 "OUT" H 1550 7076 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 1500 6750 50  0001 C CNN
F 3 "~" H 1500 6750 50  0001 C CNN
	1    1500 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6150 1150 6150
Text Label 1150 6150 2    50   ~ 0
CLK
Wire Wire Line
	1150 6950 1300 6950
Text Label 1150 6950 2    50   ~ 0
CLK
$Comp
L power:GND #PWR07
U 1 1 5E59D884
P 1850 6200
F 0 "#PWR07" H 1850 5950 50  0001 C CNN
F 1 "GND" H 1855 6027 50  0000 C CNN
F 2 "" H 1850 6200 50  0001 C CNN
F 3 "" H 1850 6200 50  0001 C CNN
	1    1850 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 6200 1850 6150
Wire Wire Line
	1850 6150 1800 6150
$Comp
L power:GND #PWR08
U 1 1 5E59DFD7
P 1850 7000
F 0 "#PWR08" H 1850 6750 50  0001 C CNN
F 1 "GND" H 1855 6827 50  0000 C CNN
F 2 "" H 1850 7000 50  0001 C CNN
F 3 "" H 1850 7000 50  0001 C CNN
	1    1850 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 7000 1850 6950
Wire Wire Line
	1850 6950 1800 6950
$Comp
L Transistor_BJT:BC817 Q1
U 1 1 5E59F0CA
P 1500 1750
F 0 "Q1" H 1691 1796 50  0000 L CNN
F 1 "BC817" H 1691 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1700 1675 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 1500 1750 50  0001 L CNN
	1    1500 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5750 1150 5750
Text Label 1150 5750 2    50   ~ 0
IN_1
Wire Wire Line
	1800 5750 1950 5750
Text Label 1950 5750 0    50   ~ 0
IN_2
Wire Wire Line
	1300 5850 1150 5850
Text Label 1150 5850 2    50   ~ 0
IN_3
Wire Wire Line
	1800 5850 1950 5850
Text Label 1950 5850 0    50   ~ 0
IN_4
Wire Wire Line
	1300 5950 1150 5950
Text Label 1150 5950 2    50   ~ 0
IN_5
Wire Wire Line
	1800 5950 1950 5950
Text Label 1950 5950 0    50   ~ 0
IN_6
Wire Wire Line
	1300 6050 1150 6050
Text Label 1150 6050 2    50   ~ 0
IN_7
Wire Wire Line
	1800 6050 1950 6050
Text Label 1950 6050 0    50   ~ 0
IN_8
Wire Wire Line
	1300 6550 1150 6550
Text Label 1150 6550 2    50   ~ 0
OUT_1
Wire Wire Line
	1300 6650 1150 6650
Text Label 1150 6650 2    50   ~ 0
OUT_3
Wire Wire Line
	1150 6750 1300 6750
Text Label 1150 6750 2    50   ~ 0
OUT_5
Wire Wire Line
	1300 6850 1150 6850
Text Label 1150 6850 2    50   ~ 0
OUT_7
Wire Wire Line
	1800 6550 1950 6550
Text Label 1950 6550 0    50   ~ 0
OUT_2
Wire Wire Line
	1800 6650 1950 6650
Text Label 1950 6650 0    50   ~ 0
OUT_4
Wire Wire Line
	1800 6750 1950 6750
Text Label 1950 6750 0    50   ~ 0
OUT_6
Wire Wire Line
	1800 6850 1950 6850
Text Label 1950 6850 0    50   ~ 0
OUT_8
$Comp
L Device:R R1
U 1 1 5E5A5B65
P 1100 1750
F 0 "R1" V 893 1750 50  0000 C CNN
F 1 "100k" V 984 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1030 1750 50  0001 C CNN
F 3 "~" H 1100 1750 50  0001 C CNN
	1    1100 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 1750 1300 1750
Wire Wire Line
	950  1750 850  1750
Text Label 850  1750 2    50   ~ 0
IN_1
$Comp
L power:GND #PWR02
U 1 1 5E5A71FB
P 1600 2000
F 0 "#PWR02" H 1600 1750 50  0001 C CNN
F 1 "GND" H 1605 1827 50  0000 C CNN
F 2 "" H 1600 2000 50  0001 C CNN
F 3 "" H 1600 2000 50  0001 C CNN
	1    1600 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1950 1600 2000
$Comp
L Device:R R3
U 1 1 5E5A7B3B
P 1600 1300
F 0 "R3" H 1530 1254 50  0000 R CNN
F 1 "250k" H 1530 1345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1530 1300 50  0001 C CNN
F 3 "~" H 1600 1300 50  0001 C CNN
	1    1600 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 1450 1600 1500
$Comp
L power:VCC #PWR01
U 1 1 5E5A8888
P 1600 850
F 0 "#PWR01" H 1600 700 50  0001 C CNN
F 1 "VCC" H 1617 1023 50  0000 C CNN
F 2 "" H 1600 850 50  0001 C CNN
F 3 "" H 1600 850 50  0001 C CNN
	1    1600 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 850  1600 1150
$Comp
L Transistor_BJT:BC817 Q3
U 1 1 5E5A9725
P 2150 1500
F 0 "Q3" H 2341 1546 50  0000 L CNN
F 1 "BC817" H 2341 1455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2350 1425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 2150 1500 50  0001 L CNN
	1    2150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 1500 1600 1500
Connection ~ 1600 1500
Wire Wire Line
	1600 1500 1600 1550
$Comp
L power:GND #PWR010
U 1 1 5E5AA6AC
P 2250 1950
F 0 "#PWR010" H 2250 1700 50  0001 C CNN
F 1 "GND" H 2255 1777 50  0000 C CNN
F 2 "" H 2250 1950 50  0001 C CNN
F 3 "" H 2250 1950 50  0001 C CNN
	1    2250 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1700 2250 1950
$Comp
L Device:R R5
U 1 1 5E5ABBBB
P 2250 1050
F 0 "R5" H 2180 1004 50  0000 R CNN
F 1 "NC" H 2180 1095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2180 1050 50  0001 C CNN
F 3 "~" H 2250 1050 50  0001 C CNN
	1    2250 1050
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR09
U 1 1 5E5AE48F
P 2250 850
F 0 "#PWR09" H 2250 700 50  0001 C CNN
F 1 "VCC" H 2267 1023 50  0000 C CNN
F 2 "" H 2250 850 50  0001 C CNN
F 3 "" H 2250 850 50  0001 C CNN
	1    2250 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 850  2250 900 
Wire Wire Line
	2250 1200 2250 1250
Wire Wire Line
	2250 1250 2500 1250
Connection ~ 2250 1250
Wire Wire Line
	2250 1250 2250 1300
Text Label 2500 1250 0    50   ~ 0
OUT_1
$Comp
L Transistor_BJT:BC817 Q5
U 1 1 5E5C17D8
P 3750 1750
F 0 "Q5" H 3941 1796 50  0000 L CNN
F 1 "BC817" H 3941 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3950 1675 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 3750 1750 50  0001 L CNN
	1    3750 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E5C17DE
P 3350 1750
F 0 "R7" V 3143 1750 50  0000 C CNN
F 1 "100k" V 3234 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3280 1750 50  0001 C CNN
F 3 "~" H 3350 1750 50  0001 C CNN
	1    3350 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 1750 3550 1750
Wire Wire Line
	3200 1750 3100 1750
Text Label 3100 1750 2    50   ~ 0
IN_2
$Comp
L power:GND #PWR014
U 1 1 5E5C17E7
P 3850 2000
F 0 "#PWR014" H 3850 1750 50  0001 C CNN
F 1 "GND" H 3855 1827 50  0000 C CNN
F 2 "" H 3850 2000 50  0001 C CNN
F 3 "" H 3850 2000 50  0001 C CNN
	1    3850 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1950 3850 2000
$Comp
L Device:R R9
U 1 1 5E5C17EE
P 3850 1300
F 0 "R9" H 3780 1254 50  0000 R CNN
F 1 "250k" H 3780 1345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3780 1300 50  0001 C CNN
F 3 "~" H 3850 1300 50  0001 C CNN
	1    3850 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 1450 3850 1500
$Comp
L power:VCC #PWR013
U 1 1 5E5C17F5
P 3850 850
F 0 "#PWR013" H 3850 700 50  0001 C CNN
F 1 "VCC" H 3867 1023 50  0000 C CNN
F 2 "" H 3850 850 50  0001 C CNN
F 3 "" H 3850 850 50  0001 C CNN
	1    3850 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 850  3850 1150
$Comp
L Transistor_BJT:BC817 Q7
U 1 1 5E5C17FC
P 4400 1500
F 0 "Q7" H 4591 1546 50  0000 L CNN
F 1 "BC817" H 4591 1455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4600 1425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 4400 1500 50  0001 L CNN
	1    4400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1500 3850 1500
Connection ~ 3850 1500
Wire Wire Line
	3850 1500 3850 1550
$Comp
L power:GND #PWR018
U 1 1 5E5C1805
P 4500 1950
F 0 "#PWR018" H 4500 1700 50  0001 C CNN
F 1 "GND" H 4505 1777 50  0000 C CNN
F 2 "" H 4500 1950 50  0001 C CNN
F 3 "" H 4500 1950 50  0001 C CNN
	1    4500 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1700 4500 1950
$Comp
L Device:R R11
U 1 1 5E5C180C
P 4500 1050
F 0 "R11" H 4430 1004 50  0000 R CNN
F 1 "NC" H 4430 1095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4430 1050 50  0001 C CNN
F 3 "~" H 4500 1050 50  0001 C CNN
	1    4500 1050
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR017
U 1 1 5E5C1812
P 4500 850
F 0 "#PWR017" H 4500 700 50  0001 C CNN
F 1 "VCC" H 4517 1023 50  0000 C CNN
F 2 "" H 4500 850 50  0001 C CNN
F 3 "" H 4500 850 50  0001 C CNN
	1    4500 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 850  4500 900 
Wire Wire Line
	4500 1200 4500 1250
Wire Wire Line
	4500 1250 4750 1250
Connection ~ 4500 1250
Wire Wire Line
	4500 1250 4500 1300
Text Label 4750 1250 0    50   ~ 0
OUT_2
$Comp
L Transistor_BJT:BC817 Q9
U 1 1 5E5C4B01
P 5900 1750
F 0 "Q9" H 6091 1796 50  0000 L CNN
F 1 "BC817" H 6091 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6100 1675 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 5900 1750 50  0001 L CNN
	1    5900 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5E5C4B07
P 5500 1750
F 0 "R13" V 5293 1750 50  0000 C CNN
F 1 "100k" V 5384 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5430 1750 50  0001 C CNN
F 3 "~" H 5500 1750 50  0001 C CNN
	1    5500 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 1750 5700 1750
Wire Wire Line
	5350 1750 5250 1750
Text Label 5250 1750 2    50   ~ 0
IN_3
$Comp
L power:GND #PWR022
U 1 1 5E5C4B10
P 6000 2000
F 0 "#PWR022" H 6000 1750 50  0001 C CNN
F 1 "GND" H 6005 1827 50  0000 C CNN
F 2 "" H 6000 2000 50  0001 C CNN
F 3 "" H 6000 2000 50  0001 C CNN
	1    6000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1950 6000 2000
$Comp
L Device:R R15
U 1 1 5E5C4B17
P 6000 1300
F 0 "R15" H 5930 1254 50  0000 R CNN
F 1 "250k" H 5930 1345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5930 1300 50  0001 C CNN
F 3 "~" H 6000 1300 50  0001 C CNN
	1    6000 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 1450 6000 1500
$Comp
L power:VCC #PWR021
U 1 1 5E5C4B1E
P 6000 850
F 0 "#PWR021" H 6000 700 50  0001 C CNN
F 1 "VCC" H 6017 1023 50  0000 C CNN
F 2 "" H 6000 850 50  0001 C CNN
F 3 "" H 6000 850 50  0001 C CNN
	1    6000 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 850  6000 1150
$Comp
L Transistor_BJT:BC817 Q11
U 1 1 5E5C4B25
P 6550 1500
F 0 "Q11" H 6741 1546 50  0000 L CNN
F 1 "BC817" H 6741 1455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6750 1425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 6550 1500 50  0001 L CNN
	1    6550 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1500 6000 1500
Connection ~ 6000 1500
Wire Wire Line
	6000 1500 6000 1550
$Comp
L power:GND #PWR026
U 1 1 5E5C4B2E
P 6650 1950
F 0 "#PWR026" H 6650 1700 50  0001 C CNN
F 1 "GND" H 6655 1777 50  0000 C CNN
F 2 "" H 6650 1950 50  0001 C CNN
F 3 "" H 6650 1950 50  0001 C CNN
	1    6650 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1700 6650 1950
$Comp
L Device:R R17
U 1 1 5E5C4B35
P 6650 1050
F 0 "R17" H 6580 1004 50  0000 R CNN
F 1 "NC" H 6580 1095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6580 1050 50  0001 C CNN
F 3 "~" H 6650 1050 50  0001 C CNN
	1    6650 1050
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR025
U 1 1 5E5C4B3B
P 6650 850
F 0 "#PWR025" H 6650 700 50  0001 C CNN
F 1 "VCC" H 6667 1023 50  0000 C CNN
F 2 "" H 6650 850 50  0001 C CNN
F 3 "" H 6650 850 50  0001 C CNN
	1    6650 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 850  6650 900 
Wire Wire Line
	6650 1200 6650 1250
Wire Wire Line
	6650 1250 6900 1250
Connection ~ 6650 1250
Wire Wire Line
	6650 1250 6650 1300
Text Label 6900 1250 0    50   ~ 0
OUT_3
$Comp
L Transistor_BJT:BC817 Q13
U 1 1 5E5CB930
P 8100 1750
F 0 "Q13" H 8291 1796 50  0000 L CNN
F 1 "BC817" H 8291 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8300 1675 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 8100 1750 50  0001 L CNN
	1    8100 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5E5CB936
P 7700 1750
F 0 "R19" V 7493 1750 50  0000 C CNN
F 1 "100k" V 7584 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7630 1750 50  0001 C CNN
F 3 "~" H 7700 1750 50  0001 C CNN
	1    7700 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	7850 1750 7900 1750
Wire Wire Line
	7550 1750 7450 1750
Text Label 7450 1750 2    50   ~ 0
IN_4
$Comp
L power:GND #PWR030
U 1 1 5E5CB93F
P 8200 2000
F 0 "#PWR030" H 8200 1750 50  0001 C CNN
F 1 "GND" H 8205 1827 50  0000 C CNN
F 2 "" H 8200 2000 50  0001 C CNN
F 3 "" H 8200 2000 50  0001 C CNN
	1    8200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 1950 8200 2000
$Comp
L Device:R R21
U 1 1 5E5CB946
P 8200 1300
F 0 "R21" H 8130 1254 50  0000 R CNN
F 1 "250k" H 8130 1345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 1300 50  0001 C CNN
F 3 "~" H 8200 1300 50  0001 C CNN
	1    8200 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 1450 8200 1500
$Comp
L power:VCC #PWR029
U 1 1 5E5CB94D
P 8200 850
F 0 "#PWR029" H 8200 700 50  0001 C CNN
F 1 "VCC" H 8217 1023 50  0000 C CNN
F 2 "" H 8200 850 50  0001 C CNN
F 3 "" H 8200 850 50  0001 C CNN
	1    8200 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 850  8200 1150
$Comp
L Transistor_BJT:BC817 Q15
U 1 1 5E5CB954
P 8750 1500
F 0 "Q15" H 8941 1546 50  0000 L CNN
F 1 "BC817" H 8941 1455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8950 1425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 8750 1500 50  0001 L CNN
	1    8750 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1500 8200 1500
Connection ~ 8200 1500
Wire Wire Line
	8200 1500 8200 1550
$Comp
L power:GND #PWR034
U 1 1 5E5CB95D
P 8850 1950
F 0 "#PWR034" H 8850 1700 50  0001 C CNN
F 1 "GND" H 8855 1777 50  0000 C CNN
F 2 "" H 8850 1950 50  0001 C CNN
F 3 "" H 8850 1950 50  0001 C CNN
	1    8850 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 1700 8850 1950
$Comp
L Device:R R23
U 1 1 5E5CB964
P 8850 1050
F 0 "R23" H 8780 1004 50  0000 R CNN
F 1 "NC" H 8780 1095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8780 1050 50  0001 C CNN
F 3 "~" H 8850 1050 50  0001 C CNN
	1    8850 1050
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR033
U 1 1 5E5CB96A
P 8850 850
F 0 "#PWR033" H 8850 700 50  0001 C CNN
F 1 "VCC" H 8867 1023 50  0000 C CNN
F 2 "" H 8850 850 50  0001 C CNN
F 3 "" H 8850 850 50  0001 C CNN
	1    8850 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 850  8850 900 
Wire Wire Line
	8850 1200 8850 1250
Wire Wire Line
	8850 1250 9100 1250
Connection ~ 8850 1250
Wire Wire Line
	8850 1250 8850 1300
Text Label 9100 1250 0    50   ~ 0
OUT_4
$Comp
L Transistor_BJT:BC817 Q2
U 1 1 5E609321
P 1500 3700
F 0 "Q2" H 1691 3746 50  0000 L CNN
F 1 "BC817" H 1691 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1700 3625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 1500 3700 50  0001 L CNN
	1    1500 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E609327
P 1100 3700
F 0 "R2" V 893 3700 50  0000 C CNN
F 1 "100k" V 984 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1030 3700 50  0001 C CNN
F 3 "~" H 1100 3700 50  0001 C CNN
	1    1100 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 3700 1300 3700
Wire Wire Line
	950  3700 850  3700
Text Label 850  3700 2    50   ~ 0
IN_5
$Comp
L power:GND #PWR04
U 1 1 5E609330
P 1600 3950
F 0 "#PWR04" H 1600 3700 50  0001 C CNN
F 1 "GND" H 1605 3777 50  0000 C CNN
F 2 "" H 1600 3950 50  0001 C CNN
F 3 "" H 1600 3950 50  0001 C CNN
	1    1600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3900 1600 3950
$Comp
L Device:R R4
U 1 1 5E609337
P 1600 3250
F 0 "R4" H 1530 3204 50  0000 R CNN
F 1 "250k" H 1530 3295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1530 3250 50  0001 C CNN
F 3 "~" H 1600 3250 50  0001 C CNN
	1    1600 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 3400 1600 3450
$Comp
L power:VCC #PWR03
U 1 1 5E60933E
P 1600 2800
F 0 "#PWR03" H 1600 2650 50  0001 C CNN
F 1 "VCC" H 1617 2973 50  0000 C CNN
F 2 "" H 1600 2800 50  0001 C CNN
F 3 "" H 1600 2800 50  0001 C CNN
	1    1600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2800 1600 3100
$Comp
L Transistor_BJT:BC817 Q4
U 1 1 5E609345
P 2150 3450
F 0 "Q4" H 2341 3496 50  0000 L CNN
F 1 "BC817" H 2341 3405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2350 3375 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 2150 3450 50  0001 L CNN
	1    2150 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 3450 1600 3450
Connection ~ 1600 3450
Wire Wire Line
	1600 3450 1600 3500
$Comp
L power:GND #PWR012
U 1 1 5E60934E
P 2250 3900
F 0 "#PWR012" H 2250 3650 50  0001 C CNN
F 1 "GND" H 2255 3727 50  0000 C CNN
F 2 "" H 2250 3900 50  0001 C CNN
F 3 "" H 2250 3900 50  0001 C CNN
	1    2250 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3650 2250 3900
$Comp
L Device:R R6
U 1 1 5E609355
P 2250 3000
F 0 "R6" H 2180 2954 50  0000 R CNN
F 1 "NC" H 2180 3045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2180 3000 50  0001 C CNN
F 3 "~" H 2250 3000 50  0001 C CNN
	1    2250 3000
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR011
U 1 1 5E60935B
P 2250 2800
F 0 "#PWR011" H 2250 2650 50  0001 C CNN
F 1 "VCC" H 2267 2973 50  0000 C CNN
F 2 "" H 2250 2800 50  0001 C CNN
F 3 "" H 2250 2800 50  0001 C CNN
	1    2250 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 2800 2250 2850
Wire Wire Line
	2250 3150 2250 3200
Wire Wire Line
	2250 3200 2500 3200
Connection ~ 2250 3200
Wire Wire Line
	2250 3200 2250 3250
Text Label 2500 3200 0    50   ~ 0
OUT_5
$Comp
L Transistor_BJT:BC817 Q6
U 1 1 5E609367
P 3750 3700
F 0 "Q6" H 3941 3746 50  0000 L CNN
F 1 "BC817" H 3941 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3950 3625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 3750 3700 50  0001 L CNN
	1    3750 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5E60936D
P 3350 3700
F 0 "R8" V 3143 3700 50  0000 C CNN
F 1 "100k" V 3234 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3280 3700 50  0001 C CNN
F 3 "~" H 3350 3700 50  0001 C CNN
	1    3350 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 3700 3550 3700
Wire Wire Line
	3200 3700 3100 3700
Text Label 3100 3700 2    50   ~ 0
IN_6
$Comp
L power:GND #PWR016
U 1 1 5E609376
P 3850 3950
F 0 "#PWR016" H 3850 3700 50  0001 C CNN
F 1 "GND" H 3855 3777 50  0000 C CNN
F 2 "" H 3850 3950 50  0001 C CNN
F 3 "" H 3850 3950 50  0001 C CNN
	1    3850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3900 3850 3950
$Comp
L Device:R R10
U 1 1 5E60937D
P 3850 3250
F 0 "R10" H 3780 3204 50  0000 R CNN
F 1 "250k" H 3780 3295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3780 3250 50  0001 C CNN
F 3 "~" H 3850 3250 50  0001 C CNN
	1    3850 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 3400 3850 3450
$Comp
L power:VCC #PWR015
U 1 1 5E609384
P 3850 2800
F 0 "#PWR015" H 3850 2650 50  0001 C CNN
F 1 "VCC" H 3867 2973 50  0000 C CNN
F 2 "" H 3850 2800 50  0001 C CNN
F 3 "" H 3850 2800 50  0001 C CNN
	1    3850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2800 3850 3100
$Comp
L Transistor_BJT:BC817 Q8
U 1 1 5E60938B
P 4400 3450
F 0 "Q8" H 4591 3496 50  0000 L CNN
F 1 "BC817" H 4591 3405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4600 3375 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 4400 3450 50  0001 L CNN
	1    4400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3450 3850 3450
Connection ~ 3850 3450
Wire Wire Line
	3850 3450 3850 3500
$Comp
L power:GND #PWR020
U 1 1 5E609394
P 4500 3900
F 0 "#PWR020" H 4500 3650 50  0001 C CNN
F 1 "GND" H 4505 3727 50  0000 C CNN
F 2 "" H 4500 3900 50  0001 C CNN
F 3 "" H 4500 3900 50  0001 C CNN
	1    4500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3650 4500 3900
$Comp
L Device:R R12
U 1 1 5E60939B
P 4500 3000
F 0 "R12" H 4430 2954 50  0000 R CNN
F 1 "NC" H 4430 3045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4430 3000 50  0001 C CNN
F 3 "~" H 4500 3000 50  0001 C CNN
	1    4500 3000
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR019
U 1 1 5E6093A1
P 4500 2800
F 0 "#PWR019" H 4500 2650 50  0001 C CNN
F 1 "VCC" H 4517 2973 50  0000 C CNN
F 2 "" H 4500 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2800 4500 2850
Wire Wire Line
	4500 3150 4500 3200
Wire Wire Line
	4500 3200 4750 3200
Connection ~ 4500 3200
Wire Wire Line
	4500 3200 4500 3250
Text Label 4750 3200 0    50   ~ 0
OUT_6
$Comp
L Transistor_BJT:BC817 Q10
U 1 1 5E6093AD
P 5900 3700
F 0 "Q10" H 6091 3746 50  0000 L CNN
F 1 "BC817" H 6091 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6100 3625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 5900 3700 50  0001 L CNN
	1    5900 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5E6093B3
P 5500 3700
F 0 "R14" V 5293 3700 50  0000 C CNN
F 1 "100k" V 5384 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5430 3700 50  0001 C CNN
F 3 "~" H 5500 3700 50  0001 C CNN
	1    5500 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3700 5700 3700
Wire Wire Line
	5350 3700 5250 3700
Text Label 5250 3700 2    50   ~ 0
IN_7
$Comp
L power:GND #PWR024
U 1 1 5E6093BC
P 6000 3950
F 0 "#PWR024" H 6000 3700 50  0001 C CNN
F 1 "GND" H 6005 3777 50  0000 C CNN
F 2 "" H 6000 3950 50  0001 C CNN
F 3 "" H 6000 3950 50  0001 C CNN
	1    6000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3900 6000 3950
$Comp
L Device:R R16
U 1 1 5E6093C3
P 6000 3250
F 0 "R16" H 5930 3204 50  0000 R CNN
F 1 "250k" H 5930 3295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5930 3250 50  0001 C CNN
F 3 "~" H 6000 3250 50  0001 C CNN
	1    6000 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 3400 6000 3450
$Comp
L power:VCC #PWR023
U 1 1 5E6093CA
P 6000 2800
F 0 "#PWR023" H 6000 2650 50  0001 C CNN
F 1 "VCC" H 6017 2973 50  0000 C CNN
F 2 "" H 6000 2800 50  0001 C CNN
F 3 "" H 6000 2800 50  0001 C CNN
	1    6000 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2800 6000 3100
$Comp
L Transistor_BJT:BC817 Q12
U 1 1 5E6093D1
P 6550 3450
F 0 "Q12" H 6741 3496 50  0000 L CNN
F 1 "BC817" H 6741 3405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6750 3375 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 6550 3450 50  0001 L CNN
	1    6550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3450 6000 3450
Connection ~ 6000 3450
Wire Wire Line
	6000 3450 6000 3500
$Comp
L power:GND #PWR028
U 1 1 5E6093DA
P 6650 3900
F 0 "#PWR028" H 6650 3650 50  0001 C CNN
F 1 "GND" H 6655 3727 50  0000 C CNN
F 2 "" H 6650 3900 50  0001 C CNN
F 3 "" H 6650 3900 50  0001 C CNN
	1    6650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3650 6650 3900
$Comp
L Device:R R18
U 1 1 5E6093E1
P 6650 3000
F 0 "R18" H 6580 2954 50  0000 R CNN
F 1 "NC" H 6580 3045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6580 3000 50  0001 C CNN
F 3 "~" H 6650 3000 50  0001 C CNN
	1    6650 3000
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR027
U 1 1 5E6093E7
P 6650 2800
F 0 "#PWR027" H 6650 2650 50  0001 C CNN
F 1 "VCC" H 6667 2973 50  0000 C CNN
F 2 "" H 6650 2800 50  0001 C CNN
F 3 "" H 6650 2800 50  0001 C CNN
	1    6650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2800 6650 2850
Wire Wire Line
	6650 3150 6650 3200
Wire Wire Line
	6650 3200 6900 3200
Connection ~ 6650 3200
Wire Wire Line
	6650 3200 6650 3250
Text Label 6900 3200 0    50   ~ 0
OUT_7
$Comp
L Transistor_BJT:BC817 Q14
U 1 1 5E6093F3
P 8100 3700
F 0 "Q14" H 8291 3746 50  0000 L CNN
F 1 "BC817" H 8291 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8300 3625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 8100 3700 50  0001 L CNN
	1    8100 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5E6093F9
P 7700 3700
F 0 "R20" V 7493 3700 50  0000 C CNN
F 1 "100k" V 7584 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7630 3700 50  0001 C CNN
F 3 "~" H 7700 3700 50  0001 C CNN
	1    7700 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	7850 3700 7900 3700
Wire Wire Line
	7550 3700 7450 3700
Text Label 7450 3700 2    50   ~ 0
IN_8
$Comp
L power:GND #PWR032
U 1 1 5E609402
P 8200 3950
F 0 "#PWR032" H 8200 3700 50  0001 C CNN
F 1 "GND" H 8205 3777 50  0000 C CNN
F 2 "" H 8200 3950 50  0001 C CNN
F 3 "" H 8200 3950 50  0001 C CNN
	1    8200 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3900 8200 3950
$Comp
L Device:R R22
U 1 1 5E609409
P 8200 3250
F 0 "R22" H 8130 3204 50  0000 R CNN
F 1 "250k" H 8130 3295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 3250 50  0001 C CNN
F 3 "~" H 8200 3250 50  0001 C CNN
	1    8200 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 3400 8200 3450
$Comp
L power:VCC #PWR031
U 1 1 5E609410
P 8200 2800
F 0 "#PWR031" H 8200 2650 50  0001 C CNN
F 1 "VCC" H 8217 2973 50  0000 C CNN
F 2 "" H 8200 2800 50  0001 C CNN
F 3 "" H 8200 2800 50  0001 C CNN
	1    8200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2800 8200 3100
$Comp
L Transistor_BJT:BC817 Q16
U 1 1 5E609417
P 8750 3450
F 0 "Q16" H 8941 3496 50  0000 L CNN
F 1 "BC817" H 8941 3405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8950 3375 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 8750 3450 50  0001 L CNN
	1    8750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3450 8200 3450
Connection ~ 8200 3450
Wire Wire Line
	8200 3450 8200 3500
$Comp
L power:GND #PWR036
U 1 1 5E609420
P 8850 3900
F 0 "#PWR036" H 8850 3650 50  0001 C CNN
F 1 "GND" H 8855 3727 50  0000 C CNN
F 2 "" H 8850 3900 50  0001 C CNN
F 3 "" H 8850 3900 50  0001 C CNN
	1    8850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3650 8850 3900
$Comp
L Device:R R24
U 1 1 5E609427
P 8850 3000
F 0 "R24" H 8780 2954 50  0000 R CNN
F 1 "NC" H 8780 3045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8780 3000 50  0001 C CNN
F 3 "~" H 8850 3000 50  0001 C CNN
	1    8850 3000
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR035
U 1 1 5E60942D
P 8850 2800
F 0 "#PWR035" H 8850 2650 50  0001 C CNN
F 1 "VCC" H 8867 2973 50  0000 C CNN
F 2 "" H 8850 2800 50  0001 C CNN
F 3 "" H 8850 2800 50  0001 C CNN
	1    8850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2800 8850 2850
Wire Wire Line
	8850 3150 8850 3200
Wire Wire Line
	8850 3200 9100 3200
Connection ~ 8850 3200
Wire Wire Line
	8850 3200 8850 3250
Text Label 9100 3200 0    50   ~ 0
OUT_8
$EndSCHEMATC
