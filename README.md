# About
 * Simple analog buffer designed for low-cost logic analyzer, but can be used
   whatever purpose :)

 ![Preview](TRNSTR_BUFF-00_preview.jpg)
 
 ![Schematics](TRNSTR_BUFF-00_sch.jpg)

 * Project is done in KiCad, but you can refer for
   [exported data](TRNSTR_BUFF-00_export.pdf)
